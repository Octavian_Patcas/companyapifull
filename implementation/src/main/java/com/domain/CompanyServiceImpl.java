package com.domain;

import com.service.CompanyService;
import com.dao.Company;
import com.dtos.CompanyDto;
import com.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dao.CompanyRepository;


import java.util.ArrayList;
import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {


    CompanyRepository companyRepository;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<CompanyDto> getAllCompanies(){
        List<CompanyDto> companies = new ArrayList<>();
        companyRepository.findAll()
                .forEach(company -> {
                    companies.add(new CompanyDto(company.getId(),company.getName(),company.getLocation()));
                });
        return companies;
    }

    public CompanyDto getCompany(Long id){
        try{
            CompanyDto companyDto = new CompanyDto();
            companyRepository.findById(id).map(company -> {
                companyDto.setName(company.getName());
                companyDto.setLocation(company.getLocation());
                return companyDto;
            });
            return companyDto;
            }
        catch (Exception e){
            throw new ResourceNotFoundException(e.getMessage());
        }
    }

    public void addCompany(CompanyDto companyDto){
        Company company = new Company();
        company.setName(companyDto.getName());
        company.setLocation(companyDto.getLocation());
        companyRepository.save(company);
    }

    public void updateCompany(Long id,CompanyDto companyDto){
        try{
            Company company = companyRepository.findById(id).get();
            company.setName(companyDto.getName());
            company.setLocation(companyDto.getLocation());
            companyRepository.save(company);
            }
        catch (Exception e){
            throw new ResourceNotFoundException(e.getMessage());
        }

    }

    public void deleteCompany(Long id){
        try{companyRepository.deleteById(id);}
        catch (Exception e){
            throw new ResourceNotFoundException(e.getMessage());
        }

    }
}
