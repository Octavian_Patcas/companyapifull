package com.domain;

import com.service.DepartmentService;
import com.dao.Company;
import com.dao.Department;
import com.dtos.CompanyDto;
import com.dtos.DepartmentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dao.DepartmentRepository;

import java.util.ArrayList;
import java.util.List;
@Service
public class DepartmentServiceImpl implements DepartmentService {

    DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    public List<DepartmentDto> getAllDepartments(Long companyId){
        List<DepartmentDto> departments = new ArrayList<>();
        departmentRepository.findByCompanyId(companyId)
                .forEach(department -> {
                    CompanyDto companyDto = new CompanyDto();
                    Company company = department.getCompany();
                    companyDto.setId(companyId);
                    companyDto.setName(company.getName());
                    companyDto.setLocation(company.getLocation());
                    departments.add(new DepartmentDto(department.getId(),department.getName(),department.getFloor(),companyDto));});
        return departments;
    }

    public DepartmentDto getDepartment(Long id,Long companyId){
        DepartmentDto departmentDto = new DepartmentDto();
        CompanyDto companyDto = new CompanyDto();
        departmentRepository.findById(id).map(department -> {
            Company company = department.getCompany();
            companyDto.setId(companyId);
            companyDto.setName(company.getName());
            companyDto.setLocation(company.getLocation());
            departmentDto.setCompanyDto(companyDto);
            departmentDto.setId(department.getId());
            departmentDto.setFloor(department.getFloor());
            departmentDto.setName(department.getName());
            return departmentDto;
        });
        return departmentDto;
    }

    @Override
    public void addDepartment(DepartmentDto departmentDto, Long companyId) {
        Department department = new Department();
        Company company = new Company();
//        company.setLocation(departmentDto.getCompanyDto().getLocation());
//        company.setName(departmentDto.getCompanyDto().getName());
        company.setId(companyId);
        department.setCompany(company);
        department.setId(departmentDto.getId());
        department.setFloor(departmentDto.getFloor());
        department.setName(departmentDto.getName());
        departmentRepository.save(department);
    }

    @Override
    public void updateDepartment(DepartmentDto departmentDto,Long id) {
        Department department = departmentRepository.findById(id).get();
        department.setId(id);
        department.setName(departmentDto.getName());
        department.setFloor(departmentDto.getFloor());
        departmentRepository.save(department);
    }

    @Override
    public void deleteDepartment(Long id) {
        departmentRepository.deleteById(id);
    }


}
