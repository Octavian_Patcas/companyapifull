package com.service;

import com.dtos.CompanyDto;

import java.util.List;

public interface CompanyService {

    List<CompanyDto> getAllCompanies();

    CompanyDto getCompany(Long id);

    void addCompany(CompanyDto companyDto);

    void updateCompany(Long id,CompanyDto companyDto);

    void deleteCompany(Long id);
}
