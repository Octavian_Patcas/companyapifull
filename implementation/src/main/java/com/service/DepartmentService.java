package com.service;

import com.dtos.DepartmentDto;

import java.util.List;

public interface DepartmentService {
    List<DepartmentDto> getAllDepartments(Long companyId);

    DepartmentDto getDepartment(Long id,Long companyId);

    void addDepartment(DepartmentDto departmentDto,Long companyId);

    void updateDepartment(DepartmentDto departmentDto,Long id);

    void deleteDepartment(Long id);
}
