package com.controllers;

import com.controllersinterfaces.CompanyController;
import com.service.CompanyService;
import com.dtos.CompanyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CompanyControllerImpl implements CompanyController {

    private CompanyService companyService;

    @Autowired
    public CompanyControllerImpl(CompanyService companyService) {
        this.companyService = companyService;
    }

    @Override
    public List<CompanyDto> getAllCompanies() {
        return companyService.getAllCompanies();
    }

    @Override
    public CompanyDto getCompany(Long id) {
        return companyService.getCompany(id);
    }

    @Override
    public void addCompany(CompanyDto companyDto) {
        companyService.addCompany(companyDto);
    }

    @Override
    public void updateCompany(CompanyDto companyDto, Long id) {
        companyService.updateCompany(id,companyDto);
    }

    @Override
    public void deleteCompany(Long id) {
        companyService.deleteCompany(id);
    }
}
