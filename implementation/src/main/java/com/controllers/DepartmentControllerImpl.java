package com.controllers;

import com.controllersinterfaces.DepartmentController;
import com.service.DepartmentService;
import com.dtos.DepartmentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
public class DepartmentControllerImpl implements DepartmentController {

    DepartmentService departmentService;

    @Autowired
    public DepartmentControllerImpl(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @Override
    public List<DepartmentDto> getAllDepartments(Long id) {
        return departmentService.getAllDepartments(id);
    }

    @Override
    public DepartmentDto getDepartment(Long id,Long companyId) {
        return departmentService.getDepartment(id,companyId);
    }

    @Override
    public void addDepartment(DepartmentDto departmentDto, Long id) {
    //    departmentDto.setCompanyDto(new CompanyDto(id,"",""));
        departmentService.addDepartment(departmentDto,id);
    }

    @Override
    public void updateDepartment(DepartmentDto departmentDto, Long companyId, Long id) {
    //    departmentDto.setCompanyDto(new CompanyDto(companyId,"",""));
        departmentService.updateDepartment(departmentDto,id);
    }

    @Override
    public void deleteDepartment(Long id) {
        departmentService.deleteDepartment(id);
    }
}
