package com.dao;

import javax.persistence.*;

@Entity
public class Department {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private Long floor;
    @ManyToOne
    private Company company;

    public Department(){

    }
    public Department(Long id,String name, Long floor,Company company) {
        super();
        this.id=id;
        this.name = name;
        this.floor = floor;
        this.company= company;
    }

    public Long getFloor() {
        return floor;
    }

    public void setFloor(Long floor) {
        this.floor = floor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
