package com.controllersinterfaces;
;
import com.dtos.DepartmentDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RequestMapping("api/v1/companies")
public interface DepartmentController {
    @GetMapping("/{id}/departments")
    @ResponseStatus(HttpStatus.OK)
     List<DepartmentDto> getAllDepartments(@PathVariable Long id);

    @GetMapping("/{companyId}/departments/{id}")
    @ResponseStatus(HttpStatus.OK)
    DepartmentDto getDepartment(@PathVariable Long id,@PathVariable Long companyId);

    @PostMapping("/{id}/departments")
    @ResponseStatus(HttpStatus.CREATED)
    void addDepartment(@RequestBody DepartmentDto departmentDto, @PathVariable Long id);

    @PutMapping("/{companyId}/departments/{id}")
    @ResponseStatus(HttpStatus.OK)
    void updateDepartment(@RequestBody DepartmentDto departmentDto,@PathVariable Long companyId,@PathVariable Long id);

    @DeleteMapping("/{companyId}/departments/{id}")
    @ResponseStatus(HttpStatus.OK)
    void deleteDepartment(@PathVariable Long id);
}
