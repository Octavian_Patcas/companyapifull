package com.controllersinterfaces;

import com.dtos.CompanyDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RequestMapping("api/v1")
public interface CompanyController {

    @GetMapping("/companies")
    @ResponseStatus(HttpStatus.OK)
    List<CompanyDto> getAllCompanies();

    @GetMapping("/companies/{id}")
    @ResponseStatus(HttpStatus.OK)
    CompanyDto getCompany(@PathVariable Long id);

    @PostMapping("/companies")
    @ResponseStatus(HttpStatus.CREATED)
    void addCompany(@RequestBody CompanyDto companyDto);

    @PutMapping("/companies/{id}")
    @ResponseStatus(HttpStatus.OK)
    void updateCompany(@RequestBody CompanyDto companyDto,@PathVariable Long id);

    @DeleteMapping("/companies/{id}")
    @ResponseStatus(HttpStatus.OK)
    void deleteCompany(@PathVariable Long id);
}
