package com.dtos;

public class DepartmentDto {

    private Long id;
    private String name;
    private Long floor;
    private CompanyDto companyDto;

    public DepartmentDto(){}
    public DepartmentDto(Long id,String name, Long floor, CompanyDto companyDto) {
        this.id = id;
        this.name = name;
        this.floor = floor;
        this.companyDto= companyDto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFloor() {
        return floor;
    }

    public void setFloor(Long floor) {
        this.floor = floor;
    }

    public CompanyDto getCompanyDto() {
        return companyDto;
    }

    public void setCompanyDto(CompanyDto companyDto) {
        this.companyDto = companyDto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
