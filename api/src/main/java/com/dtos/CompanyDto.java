package com.dtos;

public class CompanyDto {
    private Long id;
    private String name;
    private String location;

    public CompanyDto(){}

    public CompanyDto(Long id,String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
